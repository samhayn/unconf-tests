<?php

namespace Helper;

class IniFileReaderHelper extends \Codeception\Module
{
    public function seeValueInLog($value)
    {
        $filePath = PROJECT_ROOT . '/log.json';
        if (!is_file($filePath)) {
            $this->fail(sprintf('cannot find log file %s', $filePath));
        }
        $file = file_get_contents($filePath);
        $data = json_decode($file, 1);
        if (!array_intersect($data, $value)) {
            $this->fail(sprintf('Unable to find value "%s" in logs %s', $value, $data));
        }
    }
}
