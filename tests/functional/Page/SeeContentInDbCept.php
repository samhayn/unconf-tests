<?php 
$I = new FunctionalTester($scenario);
$slug = 'my_post' . time();
$lastId = $I->grabNumRecords('symfony_demo_post');
$I->haveInDatabase('symfony_demo_post', [
    'id' => ++$lastId,
    'title' => 'brand new post',
    'slug' => $slug,
    'summary' => 'lorem ipsum',
    'content' => 'Lorem ipsum dolor sit amet',
    'authoremail' => 'test@test.com',
    'publishedat' => date('Y-m-d'),
]);
$I->wantTo('perform actions and see result');
$I->sendPOST('/en/blog/comment/' . $slug . '/new', ['email' => 'try@email.com', 'content' => 'this is awesome']);
$I->seeInDatabase('symfony_demo_comment', [
    'post_id' => $lastId,
    'content' => 'this is awesome',
    'authoremail' => 'try@email.com'
]);

$I->sendPOST('/en/blog/comment/' . $slug . '/new', ['email' => 'try@email.com', 'content' => 'this is awesome']);
$I->runShellCommand(PROJECT_ROOT . '/bin/console app:remove-comments');

$comments = $I->grabNumRecords('symfony_demo_comment');
$I->assertEquals(0, $comments);
$I->canSeeValueInLog(['deleted' => 2]);
