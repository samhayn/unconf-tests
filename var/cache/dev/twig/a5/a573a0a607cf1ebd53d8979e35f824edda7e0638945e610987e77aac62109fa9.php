<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_70d35df29f4ccdf94b2bba71c251bf2b7e058a21c2ec09f21209ad88c7e1318c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f467054b3b629942a377dce17e3ffc909807d9d388d42d2ef93e4a2f5175010 = $this->env->getExtension("native_profiler");
        $__internal_2f467054b3b629942a377dce17e3ffc909807d9d388d42d2ef93e4a2f5175010->enter($__internal_2f467054b3b629942a377dce17e3ffc909807d9d388d42d2ef93e4a2f5175010_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f467054b3b629942a377dce17e3ffc909807d9d388d42d2ef93e4a2f5175010->leave($__internal_2f467054b3b629942a377dce17e3ffc909807d9d388d42d2ef93e4a2f5175010_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b8dc6fdae68822a8817c6a1275194d6f3262a159a503752234dd4c4e4d776016 = $this->env->getExtension("native_profiler");
        $__internal_b8dc6fdae68822a8817c6a1275194d6f3262a159a503752234dd4c4e4d776016->enter($__internal_b8dc6fdae68822a8817c6a1275194d6f3262a159a503752234dd4c4e4d776016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_b8dc6fdae68822a8817c6a1275194d6f3262a159a503752234dd4c4e4d776016->leave($__internal_b8dc6fdae68822a8817c6a1275194d6f3262a159a503752234dd4c4e4d776016_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_a3c10086265f140ba4c86ae098417ffb36248c275090627ca4d5d1e5d01b28b2 = $this->env->getExtension("native_profiler");
        $__internal_a3c10086265f140ba4c86ae098417ffb36248c275090627ca4d5d1e5d01b28b2->enter($__internal_a3c10086265f140ba4c86ae098417ffb36248c275090627ca4d5d1e5d01b28b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_a3c10086265f140ba4c86ae098417ffb36248c275090627ca4d5d1e5d01b28b2->leave($__internal_a3c10086265f140ba4c86ae098417ffb36248c275090627ca4d5d1e5d01b28b2_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a6aa784d9907f58e78b2b10de3b28fe34f9247db052eb536fc16217169880f35 = $this->env->getExtension("native_profiler");
        $__internal_a6aa784d9907f58e78b2b10de3b28fe34f9247db052eb536fc16217169880f35->enter($__internal_a6aa784d9907f58e78b2b10de3b28fe34f9247db052eb536fc16217169880f35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_a6aa784d9907f58e78b2b10de3b28fe34f9247db052eb536fc16217169880f35->leave($__internal_a6aa784d9907f58e78b2b10de3b28fe34f9247db052eb536fc16217169880f35_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
