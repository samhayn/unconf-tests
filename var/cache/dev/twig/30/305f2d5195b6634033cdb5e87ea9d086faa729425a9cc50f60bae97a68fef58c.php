<?php

/* default/homepage.html.twig */
class __TwigTemplate_8675dd860f3403b5c78ec3afa9e79756c14b6629ae78f63c4ff088a6f054be59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_788b6c61ace94b806569b57d1266e6b0984edb700e95b5d342b85e6e210e5638 = $this->env->getExtension("native_profiler");
        $__internal_788b6c61ace94b806569b57d1266e6b0984edb700e95b5d342b85e6e210e5638->enter($__internal_788b6c61ace94b806569b57d1266e6b0984edb700e95b5d342b85e6e210e5638_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_788b6c61ace94b806569b57d1266e6b0984edb700e95b5d342b85e6e210e5638->leave($__internal_788b6c61ace94b806569b57d1266e6b0984edb700e95b5d342b85e6e210e5638_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_44bef81e64cbc2f9bde9e7ae91f47fd1d57871f8713897444eaa854f6e1044e2 = $this->env->getExtension("native_profiler");
        $__internal_44bef81e64cbc2f9bde9e7ae91f47fd1d57871f8713897444eaa854f6e1044e2->enter($__internal_44bef81e64cbc2f9bde9e7ae91f47fd1d57871f8713897444eaa854f6e1044e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_44bef81e64cbc2f9bde9e7ae91f47fd1d57871f8713897444eaa854f6e1044e2->leave($__internal_44bef81e64cbc2f9bde9e7ae91f47fd1d57871f8713897444eaa854f6e1044e2_prof);

    }

    // line 9
    public function block_header($context, array $blocks = array())
    {
        $__internal_90f1c9f356e6c63b5961ada0069350088dc7cc95208259dc834dc9dcc4a69b9d = $this->env->getExtension("native_profiler");
        $__internal_90f1c9f356e6c63b5961ada0069350088dc7cc95208259dc834dc9dcc4a69b9d->enter($__internal_90f1c9f356e6c63b5961ada0069350088dc7cc95208259dc834dc9dcc4a69b9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_90f1c9f356e6c63b5961ada0069350088dc7cc95208259dc834dc9dcc4a69b9d->leave($__internal_90f1c9f356e6c63b5961ada0069350088dc7cc95208259dc834dc9dcc4a69b9d_prof);

    }

    // line 10
    public function block_footer($context, array $blocks = array())
    {
        $__internal_3ae83d794bf5ec00e70e8a08af51e81e3cc79608d7510376395e51d29076f9cf = $this->env->getExtension("native_profiler");
        $__internal_3ae83d794bf5ec00e70e8a08af51e81e3cc79608d7510376395e51d29076f9cf->enter($__internal_3ae83d794bf5ec00e70e8a08af51e81e3cc79608d7510376395e51d29076f9cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        
        $__internal_3ae83d794bf5ec00e70e8a08af51e81e3cc79608d7510376395e51d29076f9cf->leave($__internal_3ae83d794bf5ec00e70e8a08af51e81e3cc79608d7510376395e51d29076f9cf_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_4ad0b8e3168170986e9ac62a98a6bb8f8a73b5a16cc4fc3fae7c408d4ea2fcdf = $this->env->getExtension("native_profiler");
        $__internal_4ad0b8e3168170986e9ac62a98a6bb8f8a73b5a16cc4fc3fae7c408d4ea2fcdf->enter($__internal_4ad0b8e3168170986e9ac62a98a6bb8f8a73b5a16cc4fc3fae7c408d4ea2fcdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "    <div class=\"page-header\">
        <h1>";
        // line 14
        echo $this->env->getExtension('translator')->trans("title.homepage");
        echo "</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 21
        echo $this->env->getExtension('translator')->trans("help.browse_app");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("blog_index");
        echo "\">
                        <i class=\"fa fa-users\"></i> ";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("action.browse_app"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-6\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 34
        echo $this->env->getExtension('translator')->trans("help.browse_admin");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("admin_index");
        echo "\">
                        <i class=\"fa fa-lock\"></i> ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("action.browse_admin"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_4ad0b8e3168170986e9ac62a98a6bb8f8a73b5a16cc4fc3fae7c408d4ea2fcdf->leave($__internal_4ad0b8e3168170986e9ac62a98a6bb8f8a73b5a16cc4fc3fae7c408d4ea2fcdf_prof);

    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 38,  118 => 37,  112 => 34,  100 => 25,  96 => 24,  90 => 21,  80 => 14,  77 => 13,  71 => 12,  60 => 10,  49 => 9,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body_id 'homepage' %}*/
/* */
/* {#*/
/*     the homepage is a special page which displays neither a header nor a footer.*/
/*     this is done with the 'trick' of defining empty Twig blocks without any content*/
/* #}*/
/* {% block header %}{% endblock %}*/
/* {% block footer %}{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div class="page-header">*/
/*         <h1>{{ 'title.homepage'|trans|raw }}</h1>*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <div class="col-sm-6">*/
/*             <div class="jumbotron">*/
/*                 <p>*/
/*                     {{ 'help.browse_app'|trans|raw }}*/
/*                 </p>*/
/*                 <p>*/
/*                     <a class="btn btn-primary btn-lg" href="{{ path('blog_index') }}">*/
/*                         <i class="fa fa-users"></i> {{ 'action.browse_app'|trans }}*/
/*                     </a>*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="col-sm-6">*/
/*             <div class="jumbotron">*/
/*                 <p>*/
/*                     {{ 'help.browse_admin'|trans|raw }}*/
/*                 </p>*/
/*                 <p>*/
/*                     <a class="btn btn-primary btn-lg" href="{{ path('admin_index') }}">*/
/*                         <i class="fa fa-lock"></i> {{ 'action.browse_admin'|trans }}*/
/*                     </a>*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
