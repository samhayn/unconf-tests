<?php

/* blog/index.html.twig */
class __TwigTemplate_3a26df7738c9da60cb1a30b079eb25cbe63b1591f433b2afd448d086553e94aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "blog/index.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_454030af40d9b8a62f18b85a22f08c946caddf43fb697da4d7092f74ccce74fc = $this->env->getExtension("native_profiler");
        $__internal_454030af40d9b8a62f18b85a22f08c946caddf43fb697da4d7092f74ccce74fc->enter($__internal_454030af40d9b8a62f18b85a22f08c946caddf43fb697da4d7092f74ccce74fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_454030af40d9b8a62f18b85a22f08c946caddf43fb697da4d7092f74ccce74fc->leave($__internal_454030af40d9b8a62f18b85a22f08c946caddf43fb697da4d7092f74ccce74fc_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_5ae9655ab1e2e9010afeb10ba576a3d2a9b8f5f109400f4f2a5dbba8d756468a = $this->env->getExtension("native_profiler");
        $__internal_5ae9655ab1e2e9010afeb10ba576a3d2a9b8f5f109400f4f2a5dbba8d756468a->enter($__internal_5ae9655ab1e2e9010afeb10ba576a3d2a9b8f5f109400f4f2a5dbba8d756468a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "blog_index";
        
        $__internal_5ae9655ab1e2e9010afeb10ba576a3d2a9b8f5f109400f4f2a5dbba8d756468a->leave($__internal_5ae9655ab1e2e9010afeb10ba576a3d2a9b8f5f109400f4f2a5dbba8d756468a_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_155f742f25e0fd705df150b1619eba35a76ea95b34716b8f9da873cbcd222324 = $this->env->getExtension("native_profiler");
        $__internal_155f742f25e0fd705df150b1619eba35a76ea95b34716b8f9da873cbcd222324->enter($__internal_155f742f25e0fd705df150b1619eba35a76ea95b34716b8f9da873cbcd222324_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 7
            echo "        <article class=\"post\">
            <h2>
                <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("blog_post", array("slug" => $this->getAttribute($context["post"], "slug", array()))), "html", null, true);
            echo "\">
                    ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "
                </a>
            </h2>

            ";
            // line 14
            echo $this->env->getExtension('app.extension')->markdownToHtml($this->getAttribute($context["post"], "summary", array()));
            echo "
        </article>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 17
            echo "        <div class=\"well\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("post.no_posts_found"), "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    <div class=\"navigation text-center\">
        ";
        // line 21
        echo $this->env->getExtension('pagerfanta')->renderPagerfanta((isset($context["posts"]) ? $context["posts"] : $this->getContext($context, "posts")), "twitter_bootstrap3_translated", array("routeName" => "blog_index_paginated"));
        echo "
    </div>
";
        
        $__internal_155f742f25e0fd705df150b1619eba35a76ea95b34716b8f9da873cbcd222324->leave($__internal_155f742f25e0fd705df150b1619eba35a76ea95b34716b8f9da873cbcd222324_prof);

    }

    // line 25
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_add4dce9898bd8827e2c1196228637016b0ac2a55ba656fefb6bc2d2d3d03aea = $this->env->getExtension("native_profiler");
        $__internal_add4dce9898bd8827e2c1196228637016b0ac2a55ba656fefb6bc2d2d3d03aea->enter($__internal_add4dce9898bd8827e2c1196228637016b0ac2a55ba656fefb6bc2d2d3d03aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 26
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 28
        echo $this->env->getExtension('code_explorer_source_code')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_add4dce9898bd8827e2c1196228637016b0ac2a55ba656fefb6bc2d2d3d03aea->leave($__internal_add4dce9898bd8827e2c1196228637016b0ac2a55ba656fefb6bc2d2d3d03aea_prof);

    }

    public function getTemplateName()
    {
        return "blog/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 28,  112 => 26,  106 => 25,  96 => 21,  92 => 19,  83 => 17,  75 => 14,  68 => 10,  64 => 9,  60 => 7,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body_id 'blog_index' %}*/
/* */
/* {% block main %}*/
/*     {% for post in posts %}*/
/*         <article class="post">*/
/*             <h2>*/
/*                 <a href="{{ path('blog_post', { slug: post.slug }) }}">*/
/*                     {{ post.title }}*/
/*                 </a>*/
/*             </h2>*/
/* */
/*             {{ post.summary|md2html }}*/
/*         </article>*/
/*     {% else %}*/
/*         <div class="well">{{ 'post.no_posts_found'|trans }}</div>*/
/*     {% endfor %}*/
/* */
/*     <div class="navigation text-center">*/
/*         {{ pagerfanta(posts, 'twitter_bootstrap3_translated', { routeName: 'blog_index_paginated' }) }}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block sidebar %}*/
/*     {{ parent() }}*/
/* */
/*     {{ show_source_code(_self) }}*/
/* {% endblock %}*/
/* */
