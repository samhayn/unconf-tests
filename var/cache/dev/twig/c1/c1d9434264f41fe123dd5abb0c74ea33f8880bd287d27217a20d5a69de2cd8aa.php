<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_c6afbd714cb9cd7ccd109e364d44b400b1238010181868869eb9a15e6aedb48d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23e7689bb5e001bc226c5e768d7916ec639e74d22977c6c432ba53f9f8abf7f6 = $this->env->getExtension("native_profiler");
        $__internal_23e7689bb5e001bc226c5e768d7916ec639e74d22977c6c432ba53f9f8abf7f6->enter($__internal_23e7689bb5e001bc226c5e768d7916ec639e74d22977c6c432ba53f9f8abf7f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_23e7689bb5e001bc226c5e768d7916ec639e74d22977c6c432ba53f9f8abf7f6->leave($__internal_23e7689bb5e001bc226c5e768d7916ec639e74d22977c6c432ba53f9f8abf7f6_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_08f63df624043a7a4b1d9a35571ccf48874ecd63b63a50ed727f2333ae457173 = $this->env->getExtension("native_profiler");
        $__internal_08f63df624043a7a4b1d9a35571ccf48874ecd63b63a50ed727f2333ae457173->enter($__internal_08f63df624043a7a4b1d9a35571ccf48874ecd63b63a50ed727f2333ae457173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_08f63df624043a7a4b1d9a35571ccf48874ecd63b63a50ed727f2333ae457173->leave($__internal_08f63df624043a7a4b1d9a35571ccf48874ecd63b63a50ed727f2333ae457173_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d5b8798596b121746200ea9a88b2123b8bdf308c90ee0e92b8aa00ec45a20872 = $this->env->getExtension("native_profiler");
        $__internal_d5b8798596b121746200ea9a88b2123b8bdf308c90ee0e92b8aa00ec45a20872->enter($__internal_d5b8798596b121746200ea9a88b2123b8bdf308c90ee0e92b8aa00ec45a20872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_d5b8798596b121746200ea9a88b2123b8bdf308c90ee0e92b8aa00ec45a20872->leave($__internal_d5b8798596b121746200ea9a88b2123b8bdf308c90ee0e92b8aa00ec45a20872_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_dcadf25e16e9eb103a16e02ad7cfe21a84e5d613e2717e9fbd72c5f5f4a4eb5e = $this->env->getExtension("native_profiler");
        $__internal_dcadf25e16e9eb103a16e02ad7cfe21a84e5d613e2717e9fbd72c5f5f4a4eb5e->enter($__internal_dcadf25e16e9eb103a16e02ad7cfe21a84e5d613e2717e9fbd72c5f5f4a4eb5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_dcadf25e16e9eb103a16e02ad7cfe21a84e5d613e2717e9fbd72c5f5f4a4eb5e->leave($__internal_dcadf25e16e9eb103a16e02ad7cfe21a84e5d613e2717e9fbd72c5f5f4a4eb5e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
